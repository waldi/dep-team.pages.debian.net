<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DEP-0: Introducing Debian Enhancement Proposals (DEPs)</title>

<link rel="stylesheet" href="../../style.css" type="text/css" />
<link rel="stylesheet" href="../../local.css" type="text/css" />


</head>
<body>

<div class="header">
<span>

</div>


<div class="actions">
<ul>


<li><a href="../../recentchanges/">RecentChanges</a></li>


<li><a href="http://svn.debian.org/viewvc/dep/web/deps/dep0.mdwn">History</a></li>



</ul>
</div>




<div id="content">
<pre><code>Title: Introducing Debian Enhancement Proposals (DEPs)
DEP: 0
State: CANDIDATE
Date: 2009-07-26
Drivers: Stefano Zacchiroli &lt;zack@debian.org&gt;,
 Charles Plessy &lt;plessy@debian.org&gt;,
 Ben Finney &lt;ben+debian@benfinney.id.au&gt;
URL: http://dep.debian.net/deps/dep0
Source: http://anonscm.debian.org/viewvc/dep/web/deps/dep0.mdwn
License: http://www.jclark.com/xml/copying.txt
Abstract:
 Workflow for managing discussions about improvements to Debian
 and archiving their outcomes.
</code></pre>

<h2>Introduction</h2>

<p>This is a proposal to organize discussions about Debian enhancements,
reflect their current status and, in particular, to archive their
outcomes, via a new lightweight process based on Debian Enhancement
Proposals (DEPs). This idea is loosely based on existing similar systems
such as RFCs and Python PEPs. It is also completely opt-in, and does not
involve any new committees, powers, or authorities.</p>

<h2>Motivation</h2>

<p>Currently, when having discussions about improvements to Debian, it is
not always clear when consensus has been reached, and people willing to
implement it may start too early, leading to wasted efforts, or delay it
indefinitely, because there's not clear indication it is time to begin. At the
same time, potential adopters of an enhancement may not be able to
easily assess whether they should use said implementation or not,
because it's difficult to know whether it adjusts to the consensus
reached during the discussion period.</p>

<p>Our normative documents rely on wide adoption of a practice before
documenting it, and adopters can be reluctant to make use of it before a
clear indication that a practice has some consensus behind it. This
creates a hard to break loop that this process hopes to alleviate, by
providing a mechanism to reflect the status of each proposal, including
whether it has reached consensus or not.</p>

<p>Secondly, we lack at the moment a central index in which to list such
proposals, which would be useful to see at a glance what open fronts
there are at a given moment in Debian, and who is taking care of them
and, additionally, to serve as a storage place for successfully
completed proposals, documenting the outcome of the discussion and the
details of the implementation.</p>

<p>By using this process, people involved in developing any enhancement can
help to build such index, with very little overhead required on their
part.</p>

<h2>Workflow</h2>

<p>A "Debian enhancement" can be pretty much any change to Debian,
technical or otherwise. Examples of situations when the DEP process
might be or might have been used include:</p>

<ul>
<li>Introducing new debian/control fields (Homepage, Vcs-*).</li>
<li>Making debian/copyright be machine parseable.</li>
<li>Agreeing upon a meta-package name or virtual package name.</li>
<li>Deciding on a procedure for the Debconf team for assigning travel
sponsorship money.</li>
<li>Formalizing existing informal processes or procedures, e.g.,
the procedure for getting a new architecture added to the archive, or
getting access to piatti.debian.org to run QA tests.</li>
</ul>


<p>The workflow is very simple, and is intended to be quite lightweight:
an enhancement to Debian is suggested, discussed, implemented, and
becomes accepted practice (or policy, if applicable), in the normal
Debian way. As the discussion progresses, the enhancement is assigned
certain states, as explained below. During all the process, a single URL
maintained by the proposers can be used to check the status of the
proposal.</p>

<p>The result of all this is:</p>

<ol>
<li> an implementation of the enhancement and</li>
<li> a document that can be referred to later on without having to dig
 up and read through large discussions.</li>
</ol>


<p>The actual discussions should happen in the usual forum or forums for
the topic of the DEP. This way, DEPs do not act as yet another forum to
be followed. For example, a DEP suggesting changes to www.debian.org
graphical design should happen on debian-www, as usual.</p>

<p>In the same way, DEPs do not give any extra powers or authority to
anyone: they rely on reaching consensus in the traditional Debian way,
by engaging in discussions on mailing lists, IRC, or real life meetings
as appropriate, and not by consulting an external body for a decision.
To be acceptable, this consensus includes agreement from affected
parties, including those who would have to implement it or accept an
implementation.</p>

<p>The person or people who do the suggestion are the "drivers" of the
proposal and have the responsibility of writing the initial draft, and
of updating it during the discussions, see below.</p>

<h2>Proposal states</h2>

<div style="float: right; text-align: center;">
[[!img <span class="error">Error: Image::Magick is not installed</span>]]
<br />
<span style="font-size: xx-small">DEP workflow: state diagram</span>
</div>


<p>A given DEP can be in one of the following <em>states</em>:</p>

<ul>
<li>DRAFT</li>
<li>CANDIDATE</li>
<li>ACCEPTED</li>
<li>REJECTED</li>
<li>OBSOLETE</li>
</ul>


<p>The ideal progression of states is DRAFT -> CANDIDATE -> ACCEPTED, but
reality requires a couple of other states and transitions as well.</p>

<h3>DRAFT state: discussion</h3>

<ul>
<li>every new proposal starts as a DRAFT</li>
<li>anyone can propose a draft</li>
<li>each draft has a number (next free one from document index)</li>
<li>normal discussion and changes to the text happen in this state</li>
<li>drafts should include <em>extra</em> criteria for success (in addition to
having obtained consensus, see below), that is, requirements to
finally become ACCEPTED</li>
</ul>


<h4>DRAFT -> CANDIDATE: rough consensus</h4>

<p>In order for a DEP to become CANDIDATE, the following condition should
be met:</p>

<ul>
<li>consensus exists for <em>what</em> should be done, and <em>how</em> it should be
done (agreement needs to be expressed by all affected parties, not
just the drivers; silence is not agreement, but unanimity is not
required, either)</li>
</ul>


<h3>CANDIDATE: implementation + testing</h3>

<p>The CANDIDATE state is meant to prove, via a suitable implementation
and its testing, that a given DEP is feasible.</p>

<ul>
<li>of course, implementation can start in earlier states</li>
<li>changes to the text can happen also in this period, primarily based
on feedback from implementation</li>
<li>this period must be long enough that there is consensus that the
enhancement works (on the basis of implementation evaluation)</li>
<li>since DEP are not necessarily technical, "implementation" does not
necessarily mean coding</li>
</ul>


<h4>CANDIDATE -> ACCEPTED: working implementation</h4>

<p>In order for a DEP to become ACCEPTED, the following condition should
be met:</p>

<ul>
<li>consensus exists that the implementation has been a success</li>
</ul>


<h3>ACCEPTED: have fun</h3>

<p>Once accepted:</p>

<ul>
<li>the final version of the DEP text is archived on
<a href="http://dep.debian.net">http://dep.debian.net</a> for future reference</li>
<li>if applicable, the proposed DEP change is integrated into
authoritative texts such as policy, developer's reference, etc.</li>
</ul>


<h4>{DRAFT, CANDIDATE} -> REJECTED</h4>

<p>A DEP can become REJECTED in the following cases:</p>

<ul>
<li>the drivers are no longer interested in pursuing the DEP and
explicitly acknowledge so</li>
<li>there are no modifications to a DEP in DRAFT state for 6 months or
more</li>
<li>there is no consensus either on the draft text or on the fact that
the implementation is working satisfactorily</li>
</ul>


<h4>ACCEPTED -> OBSOLETE: no longer relevant</h4>

<p>A DEP can become OBSOLETE when it is no longer relevant, for example:</p>

<ul>
<li>a new DEP gets accepted overriding previous DEPs (in that case the
new DEP should refer to the one it OBSOLETE-s)</li>
<li>the object of the enhancement is no longer in use</li>
</ul>


<h3>{REJECTED, OBSOLETE}</h3>

<p>In one of these states, no further actions are needed.</p>

<p>It is recommended that DEPs in one of these states carry a reason
describing why they have moved to such a state.</p>

<h2>What the drivers should do</h2>

<p>The only additional burden of the DEP process falls on the shoulders of its
drivers. They have to take care of all the practical work of writing
and maintaining the text, so that everyone else can just continue
discussing things as before.  Driver's burden can be summarized as:</p>

<ul>
<li>Write the draft text and update it during discussion.</li>
<li>Determine when (rough) consensus in discussion has been reached.</li>
<li>Implement, or find volunteers to implement.</li>
<li>Determine when consensus of implementation success has been reached,
when the testing of the available implementation has been satisfactory.</li>
<li>Update the DEP with progress updates at suitable intervals, until the
DEP has been accepted (or rejected).</li>
</ul>


<p>If the drivers go missing in action, other people may step in and
courteously take over the driving position.</p>

<p><strong>Note</strong>: the drivers can of course participate in the discussion as
everybody else, but have no special authority to impose their ideas to
others. <q>DEP gives pencils, not hammers.</q></p>

<h2>Format and content</h2>

<p>A DEP is basically a free-form plain text file, except that it must
start with a paragraph of the following RFC822-style headers:</p>

<ul>
<li>Title: the full title of the document</li>
<li>DEP: the number for this DEP</li>
<li>State: the current state of this revision</li>
<li>Date: the date of this revision</li>
<li>Drivers: a list of drivers (names and e-mail addresses), in RFC822
syntax for the To: header</li>
<li>URL: during DRAFT state, a link to the canonical place of the draft
(typically probably http://wiki.debian.org/DEP/DEPxxx or
http://dep.debian.net/deps/depXXX)</li>
<li>Source: one or more URLs to browse the sources used to produce the displayed
version of the DEP.</li>
<li>Abstract: a short paragraph (formatted as the long Description in
debian/control)</li>
</ul>


<p>(Additionally, REJECTED DEPs can carry a "Reason:" field describing
why they were rejected.)</p>

<p>The rest of the file is free form. If the DEP is kept in a wiki, using
its markup syntax is, of course, a good idea.</p>

<p>Suggested document contents:</p>

<ul>
<li>An introduction, giving an overview of the situation and the motivation
for the DEP.</li>
<li>A plan for implementation, especially indicating what parts of Debian need
to be changed, and preferably indicating who will do the work.</li>
<li>Preferably a list of criteria to judge whether the implementation has been
a success.</li>
<li>Links to mailing list threads, perhaps highlighting particularly important
messages. If discussion happens on IRC, pointers to logs would be nice.</li>
</ul>


<h2>License</h2>

<p>The DEP must have a license that is DFSG free. You may choose the
license freely, but the "Expat" license is recommended. The
official URL for it is <a href="http://www.jclark.com/xml/copying.txt">http://www.jclark.com/xml/copying.txt</a> and
the license text is:</p>

<pre><code>Copyright (c) &lt;year&gt;  &lt;your names&gt;

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
</code></pre>

<p>The justification for this recommendation is that this license is one
of the most permissive of the well-known licenses. By using this
license, it is easy to copy parts of the DEP to other places, such as
documentation for Debian development or embedded in individual
packages.</p>

<h2>Creating a DEP</h2>

<p>The procedure to create a DEP is simple: send an e-mail to
<code>debian-project@lists.debian.org</code>, stating that you're taking the next
available number, and including the first paragraph of the DEP as
explained above. It is very important to include the list of drivers,
and the URL where the draft will be kept up to date. The next available
DEP number can be obtained by consulting <a href="http://dep.debian.net">http://dep.debian.net</a>.</p>

<p>It is also a very good idea to mention in this mail the place where the
discussion is going to take place, with a pointer to the thread in the
mailing list archives if it has already started.</p>

<p>Additionally, drivers are welcome to maintain their DEPs, even in the
draft state, in a repository inside the <code>dep</code> Alioth project, following
the instructions at <a href="http://dep.debian.net/depdn-howto">http://dep.debian.net/depdn-howto</a>. They are free not to
do so, and in that case a DEP0 driver or some interested party will
update the <code>dep.debian.net</code> index with their DEP, and a pointer to the
URL they provided.</p>

<h2>Revising an accepted DEP</h2>

<p>If the feature, or whatever, of the DEP needs further changing later,
the process can start over with the accepted version of the DEP document
as the initial draft. The new draft will get a new DEP number. Once the
new DEP is accepted, the old one should move to OBSOLETE state.</p>

<h2>License</h2>

<p>The following copyright statement and license apply to DEP0 (this
document).</p>

<p>Copyright (c) 2008-2009  Stefano Zacchiroli, Adeodato Simó, Lars Wirzenius</p>

<p>Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:</p>

<p>The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.</p>

<p>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</p>

<h2>Changes</h2>

<ul>
<li><p>2013-12-25:
[ Charles Plessy ]</p>

<ul>
<li>DEP 0 drivers: removed Adeodato and Lars; added Ben and Charles.</li>
</ul>
</li>
<li><p>2012-03-25:
[ Charles Plessy ]</p>

<ul>
<li>Added a "Source" field, to transfer the VCS information from the index to
the DEP itself.</li>
</ul>
</li>
<li><p>2009-07-26:
[ Zack, Dato, Lars]</p>

<ul>
<li>Re-word state description: describe separately states and
transitions.</li>
<li>Rename "DROPPED" status to "REJECTED"; inhibit "resurrection" of
REJECTED DEPs.</li>
<li>Mention that dep.debian.net is the central archival place</li>
<li>Stress that DEPs give no special powers to drivers, in the "driver
role" section</li>
<li>Change the state of DEP0 to CANDIDATE</li>
</ul>
</li>
<li><p>2008-06-12:
[ Lars Wirzenius ]</p>

<ul>
<li>Added a recommendation for the Expat license for new DEPs.</li>
<li>Set this DEP to be licensed under the Expat license.</li>
</ul>
</li>
<li><p>2008-05-29:
[ Lars Wirzenius ]</p>

<ul>
<li>Added section saying that a DEP should have a DFSG free license.</li>
</ul>
</li>
<li><p>2008-01-15:
[ Adeodato Simó ]</p>

<ul>
<li>Add section about how to create a DEP.</li>
<li>Rewrite "Introduction" (splitting "Motivation" off), and parts of
"Workflow" as well.</li>
</ul>


<p>[ Lars Wirzenius ]</p>

<ul>
<li>Typo fixes.</li>
</ul>
</li>
<li><p>2008-01-11: Minor tweaks by Zack (mostly cosmetic, but also
some more detailed specification of former more vague aspects)</p></li>
<li><p>2008-01-09: Various cleanups and tweaks by Lars, based on feedback
from several parties.</p></li>
<li><p>2007-12-01: Initial version written after some quick brainstorming at
the QA meeting in Extremadura, Spain, by Stefano, Adeodato, and Lars.</p></li>
</ul>


</div>

<div id="footer">
<div id="pageinfo">







<div class="pagedate">
Last edited <span class="date">Thu, 26 Dec 2013 15:44:17 +0000</span>
</div>

</div>

<!-- from Debian Enhancement Proposals -->
</div>

</body>
</html>
